#pragma once
#include "Color.h"
#include <sstream>
using namespace std;
class Piece
{
private:
	Color color; //Color als Attribute zum Festlegen von Farben der Figuren
public:
	//Ausgabe von Figuren auf dem Schachbrett
	virtual stringstream draw() = 0;
	// Zum Festlegen der Farben der Figuren
	virtual void setcolor(int a);
	//um die Type der Figuren erhalten
	virtual Color getcolor();
};
