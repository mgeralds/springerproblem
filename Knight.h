#pragma once
#include "Piece.h"
#include "Color.h"

class Knight : public Piece
{
public:
	// Darstellung vom Springer auf dem Brett
	virtual stringstream draw();
	
};