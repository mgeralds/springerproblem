#pragma once
#include <fstream>
#include <iostream>
#include "Field.h"

#include "Graph.h"
#include "Node.h"
#include "Edge.h"
#include <sstream>
#include <string>
using namespace std;
#include<string>


class Chessboard {
private:

	//Ich teile die Arrays von chessboard in zwei: Eine mit dem Typ Field f�r Implementierung von Graph, zum anderen ist mit dem Typ char (f�r grafische Darstellung)
	Field field[8][8]; //spalte x zeile -> Array von Fields (Bezeichnung im weiteren Teilen: "FELDERARRAY")
	char printfield[25][41]; //zeile x spalte -> Array f�r die grafische Darstellung (Einzelne chars sind einzeln gestellt und einzeln im Fenster ausgedruckt)
	//Bezeichnung im weiteren Teilen: "DARSTELLUNGSARRAY"
	Graph* g; //Graph = Eine Sammlung von Field (abgeleitete Node) und Edges (zwischen Feldern)
public:
	//Datei einlesen und initialisieren
	void initialize(std::ifstream& file);
	//Ort vom wei�en Reiter und schwarzen K�nig finden (Start->Ende)
	Field findKnight();
	Field findKing();

	//Schachbrett drucken
	virtual stringstream draw();

	//Bestimmter Spielfeld kriegen
	Field getfield(int i, int j); //spalte x zeile

	//Der k�rsteste Weg drucken
	void printDikjstra();




};