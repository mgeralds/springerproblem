#include "Knight.h"
#include <sstream>
using namespace std;
stringstream Knight::draw()
{

	if (getcolor() == (Color)1) //Weisser Springer im Board als 's' sichtbar
	{
		stringstream ss;
		ss << "s";
		return ss;
	}
	
	else
	{
		stringstream ss; // schwarzer Springer im Board als 'S' sichtbar
		ss << "S";
		return ss;
	}
}
