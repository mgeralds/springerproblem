#pragma once
#include <string>
#include "Color.h"
#include "Piece.h"
#include<sstream>



#include "Node.h"


using namespace std;

class Field : public Node
{
private:
	unsigned short row; //zeile
	char column; //spalte
	Color color; //farbe
	
	//Pointer auf dem Figur, der zu Begin auf Null zeigt
	Piece* piece = nullptr; 
	char out[3][5]; //array f�r ein Feld zeile x spalte

public:

	//Funktion zum Festlegen der Farben des Feldes
	void setcolor(Color c);

	//Funktion zur Zuweisung der Figuren auf das entsprechendes Feld nach Angabe in der Datei
	void setpiece(Piece* a); 
	//Funktion als Ausgabe der Figuren
	Piece* getpiece();
	//Ausgabe von der Inhalt in der jeweilige Spalte und Zeile
	char print(int z, int s);

	//Ausgabe von Zeile
	unsigned short getrow();
	//Ausgabe von Spalte
	char getcolumn();

	//parametrisierte Konstruktoren zur Erzeugung vom Feld

	Field(unsigned short a, char b, Color c, Piece* p, string id);

	//Standardkonstruktor
	Field();
	//Destruktor
	~Field();

};
