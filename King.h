#pragma once
#include "Color.h"
#include "Piece.h"
#include <sstream>
using namespace std;
class King : public Piece
{
public:
	//Darstellung des K�nigs auf dem Brett
	virtual stringstream draw();
};