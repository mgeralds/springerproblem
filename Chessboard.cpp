#include "Chessboard.h"
#include "Field.h"
#include "Knight.h"
#include "Piece.h"
#include "Pawn.h"
#include "King.h"
#include "Graph.h"
#include "Node.h"
#include "Edge.h"
#include<iostream>

//Initialisierung des Schachbretts
void Chessboard::initialize(std::ifstream& file)
{



//Array zur Ausgabe initialisieren

	//DARSTELLUNGSARRAY leer machen
	for (int z = 0; z <= 24; z++)
	{
		for (int s = 0; s <= 40; s++)
		{
			printfield[z][s] = ' ';
		}
	}

	//Nummer stellen auf die linkeste Spale im DARSTELLUNGSARRAY 
	char nummer = 49;
	for (int z = 2; z <= 25; z += 3)
	{
		printfield[z][0] = nummer;
		nummer++;
	}
	//Buchstaben stellen auf die unterste Zeile  im DARSTELLUNGSARRAY
	char buchstaben = 97;
	for (int s = 3; s <= 41; s += 5)
	{
		printfield[0][s] = buchstaben;
		buchstaben++;
	}


//Datei einlesen 

	
	char stringfile[20]; //Platz f�r Textinhalt

	//Kann die Datei eingelesen werden? Wenn ja: es geht weiter
	if (file)
	{
		//Zugriff auf der Datei im eingegebenen Verzeichnis, der Inhalt ist in stringfile[20] reingef�gt
		file >> stringfile;
		//roher Inhalt der Datei darstellen
		cout << "Inhalt: " << stringfile << endl;



		char spalte;
		int zeile;


		//FELDERARRAY initialisieren, die Schleife l�uft 2-Dimensional (spalte x reihe) 
		for (int i = 0; i <= 7; i++)//Alphabet spalte column
		{
			for (int j = 0; j <= 7; j++) //Nummer Zeile row
			{
				//Index (Namen, ZB: a1, b6, usw), n�tig f�r Node!
				stringstream ss;
				string str;
				spalte = i + 97;
				zeile = j + 1;
				ss << spalte << zeile;
				str = ss.str();

				//FELDERARRAY initialisieren mit Fieldkonstruktor; belegt mit zeile, spalte, Farbe, nullptr f�r Piece (noch keine Figur drauf gestellt), Index f�r Node
				field[i][j] = Field((j + 1), (i + 97), (Color)((i + j) % 2), nullptr, str);



			}
		}

		//Variable f�r Figuren (wir stellen diese auf bestimmten Feldern; wie im Board.txt gefragt)
		Knight* s = new Knight;
		Pawn* b = new Pawn;
		King* k = new King;

		//p wird mit der auf einem bestimmten Field zu stellenden Figur belegt
		Piece* p = nullptr;

		//index f�r FELDERARRAY
		int i_zeile_array;
		int i_spalte_array;

		//Index f�r Textdatei 
		
		int i_figure = 0;
		int i_spalte = 1;
		int i_zeile = 2;

		/*
		Textdatei in stringfile[20]
		Das Laufzeitsystem liest das i_figure = (0+4k)-tes Char als Art der Figur
		Das Laufzeitsystem liest das i_Spalte = (1+4k)-tes Char als Position der Figur (welche Spalte?)
		Das Laufzeitsystem liest das i_zeile = (2+4k)-tes Char als Position der Figur (welche Zeile?)

		k ist nat�rliche Zahl (0 geh�rt dazu)
		*/

		//Schleife l�uft, bis leeres Zeichen gelesen ist (Ab dann steht keine Figur zur Verf�gung)
		for (int i = 0; stringfile[i_figure] != '\0'; i++)
		{

			/*
			Fallunterscheidungen
			
			Es steht 7 F�lle zur Verf�gung
			*/

			//1.Fall: Wei�er Springer
			if (stringfile[i_figure] == 's')
			{

				//Farbe des Springers setzen= Wei�
				s->setcolor(1);
				// Zu setzende Figur = Springer (Wei�!)
				p = s;


			}
			else
			{
				//2.Fall: Wei�er Bauer
				if (stringfile[i_figure] == 'b')
				{
					//Farbe des Bauers setzen= Wei�
					b->setcolor(1);
					// Zu setzende Figur = Bauer (Wei�!)
					p = b;

				}
				else
				{
					//3.Fall: Wei�er K�nig
					if (stringfile[i_figure] == 'k')
					{
						//Farbe des K�nigs setzen= Wei�
						k->setcolor(1);
						// Zu setzende Figur = K�nig (Wei�!)
						p = k;

					}
					else
					{
						//4.Fall: Schwarzer Springer
						if (stringfile[i_figure] == 'S')
						{
							//Farbe des Springers setzen= Schwarz
							s->setcolor(0);
							// Zu setzende Figur = Springer (Schwarz!)
							p = s;

						}
						else
						{
							//5.Fall: Schwarzer Bauer
							if (stringfile[i_figure] == 'B')
							{
								//Farbe des Bauers setzen= Schwarz
								b->setcolor(0);
								// Zu setzende Figur = Bauer (Schwarz!)
								p = b;


							}
							else
							{
								//6.Fall: Schwarzer K�nig
								if (stringfile[i_figure] == 'K')
								{
									//Farbe des K�nigs setzen= Schwarz
									k->setcolor(0);
									// Zu setzende Figur = K�nig (Schwarz!)
									p = k;

								}
								else
								{
									//7.Fall: nichts
									//Dieser Fall ist nicht einzutreten (Die Schleife l�uft w�hrend des Vorhandenseins der Figuren in Textdatei (
								}
							}
						}
					}
				}
			}
			//Spalte kriegen aus der Textdatei  (Der Inhalt ist als ASCII-Decimal zu betrachten)
			if ((stringfile[i_spalte] >= 97) && (stringfile[i_spalte] <= 104))
			{
				i_spalte_array = (int)(stringfile[i_spalte] - 97);
			}
			//Spalte kriegen aus der Textdatei
			if ((stringfile[i_zeile] >= 49) && (stringfile[i_zeile] <= 56))
			{
				i_zeile_array = (int)(stringfile[i_zeile] - 49);
			}

			/*
			Spalte: ZB:  spalte a (1.Spalte); a ist 97 in ASCII-Decimal (vgl. http://www.asciitable.com/)  ; Ich ziehe 97 davon. Das ergibt sich der Index 0 (field[0][..]) 
			Zeile: ZB: zeile 1 (1.Spalte); 1 ist 49 in ASCII-Decimal; Ich ziehe 49 davon. Das ergibt sich der Index 0 (field[..][0])
			*/

			// 

			/*
			Figure stellen auf Schachbrett.
			 Die Indizen f�r Zeile, Spalte sind festgelegt.
			 Die Art der Figur (p) ist schon festgelegt.

			 Die Figur wird in FELDERARRAY in der festgelegten Position (spalte, Zeile) gestellt
			*/
			if (stringfile[i_figure] != '\0') 
			{
				field[i_spalte_array][i_zeile_array].setpiece(p);
			}


			//Nach dem Durchlauf werden die Indizen um 4 chars nach rechts verschoben (vgl. Definition in Zeile 104)

			i_figure = i_figure + 4;
			i_spalte = i_spalte + 4;
			i_zeile = i_zeile + 4;
		};
		
		
		

		
		//Datei muss geschlossen werden
		file.close();


	}

	//Eine Graph wird gestellt
	g = new Graph;

	//Schleife Durchlaufen, um alle Felder (abgeleitete Node) in dem FELDERARRAY in die Graph einzuf�gen.
	for (int i = 0; i <= 7; i++) //spalte x zeile
	{
		for (int j = 0; j <= 7; j++)
		{
			
			g->addNode(&field[i][j]);

		}
	}



	//Edge machen
	for (int i = 0; i <= 7; i++)//Alphabet spalte column
	{
		for (int j = 0; j <= 7; j++) //Nummer Zeile row
		{
			//Figuren der ausgehenden (out) und eingehenden �(in) Felder sind mit nullptr initialisiert
			Piece* p_out = nullptr;
			Piece* p_in = nullptr;

			//Zeile und Spalte der eingehenden Felder
			int s_in, z_in;

			//Vergleichkarakter f�r ausgehende und eingehende Nodes 
			//Die Char in out[1][2]/ mittelre char der Darstellungsarray in Field in jeweiligen Felder
			char vergleich_out = ' ';
			char vergleich_in = ' ';

			//ab hier f�ngt die �berpr�fung der auf Field gestellter Figuren und die Gestaltung der Edges 

			//einmal links zweimal oben
			s_in = i - 1;
			z_in = j + 2;

			//Liegt das eingehende Feld (Zielfeld, abgeleitete Node) innerhalb des Schachbretts?
			if ((s_in >= 0) && (s_in <= 7) && (z_in >= 0) && (z_in <= 7))
			{
				//Wenn ja: die mittlere char (out[1][2]) der Darstellungsarray in Eingangs- und Ausgangsfield 
				vergleich_out = field[i][j].print(1, 2);
				vergleich_in = field[s_in][z_in].print(1, 2);

				//Besitzen die eingehende und ausgehende Nodes/Fielder wei�en Bauer/K�nig?
				/*
				In der Aufgabestellung steht explzit, dass der wei�e Springer nicht auf den Feldern, die *einen wei�en Bauer* enth�lt, gestellt werden darf.
				(Aufgabe 5)

				In Aufgabe 2b wird es angenommen, dass es 6 M�glichkeiten f�r Figuren gibt: {schwarzer, wei�er} {Springer, Bauer, K�nig}

				Djkstra-Algorithmus nach gibt es nur jeweils eine Start- und Endnode. 
				Das hei�t, dass es angenommen wird, dass es NUR EINEN wei�en Springer (Start) und EINEN schwarzen K�nig (Ende) gibt.

				
				Aus diesen �berlegungen wird es angenommen, dass der wei�e Springer nicht auf irgeinem Feld *mit irgendeiner wei�er Figur* gestellt werden darf.. (Wei�er Bauer und K�nig)
				, da die wei�en Figuren "Freunde" des wei�en Springers


				Im Gegensatz dazu darf die wei�en Figuren auf Feldern mit schwarzen Figuren (schwarzer K�nig, Bauer oder Springer) gestellt werden. Die schwarzen Figuren sind Gegner der wei�en Figuren.

				Nach dem Kontext in der Aufgabenstellung interessiert man nur f�r den k�rzesten Weg vom Start bis Endfeld (Node).
				Das hei�t, das "Spiel" endet, wenn der Springer den K�nig erreicht.

				
				*/

				//Edge machen, nur wenn das eingehende und ausgehende Field keinen Wei�en K�nig oder Bauer besitzt. 
				if (((vergleich_in != 'b') && (vergleich_out != 'b'))|| ((vergleich_in != 'k')&& (vergleich_out != 'k')))
				{

					Edge* edge = new Edge(field[i][j], field[s_in][z_in]);
					g->addEdge(edge);

				}

			}

			//

			p_out = nullptr;
			p_in = nullptr;
			vergleich_in = ' ';
			vergleich_out = ' ';

			//einmal rechts zweimal oben
			s_in = i + 1;
			z_in = j + 2;


			//Liegt das eingehende Feld (Zielfeld, abgeleitete Node) innerhalb des Schachbretts?
			if ((s_in >= 0) && (s_in <= 7) && (z_in >= 0) && (z_in <= 7))
			{
				//Wenn ja: die mittlere char (out[1][2]) der Darstellungsarray in Eingangs- und Ausgangsfield
				vergleich_out = field[i][j].print(1, 2);
				vergleich_in = field[s_in][z_in].print(1, 2);


				//Edge machen, nur wenn das eingehende und ausgehende Field keinen Wei�en K�nig oder Bauer besitzt. 
				if ((vergleich_in != 'b') && (vergleich_out != 'b') && (vergleich_in != 'k') && (vergleich_out != 'k'))
				{
					Edge* edge = new Edge(field[i][j], field[s_in][z_in]);
					g->addEdge(edge);
				}

			}


			p_out = nullptr;
			p_in = nullptr;
			vergleich_in = ' ';
			vergleich_out = ' ';

			//zweimal links einmal oben
			s_in = i - 2;
			z_in = j + 1;

			//Liegt das eingehende Feld (Zielfeld, abgeleitete Node) innerhalb des Schachbretts?
			if ((s_in >= 0) && (s_in <= 7) && (z_in >= 0) && (z_in <= 7))
			{

				//Wenn ja: die mittlere char (out[1][2]) der Darstellungsarray in Eingangs- und Ausgangsfield
				vergleich_out = field[i][j].print(1, 2);
				vergleich_in = field[s_in][z_in].print(1, 2);

				//Edge machen, nur wenn das eingehende und ausgehende Field keinen Wei�en K�nig oder Bauer besitzt. 
				if ((vergleich_in != 'b') && (vergleich_out != 'b') && (vergleich_in != 'k') && (vergleich_out != 'k'))
				{
					Edge* edge = new Edge(field[i][j], field[s_in][z_in]);
					g->addEdge(edge);
				}

			}


			p_out = nullptr;
			p_in = nullptr;
			vergleich_in = ' ';
			vergleich_out = ' ';

			//zweimal rechts einmal oben
			s_in = i + 2;
			z_in = j + 1;

			//Liegt das eingehende Feld (Zielfeld, abgeleitete Node) innerhalb des Schachbretts?
			if ((s_in >= 0) && (s_in <= 7) && (z_in >= 0) && (z_in <= 7))
			{
				//Wenn ja: die mittlere char (out[1][2]) der Darstellungsarray in Eingangs- und Ausgangsfield
				vergleich_out = field[i][j].print(1, 2);
				vergleich_in = field[s_in][z_in].print(1, 2);

				//Edge machen, nur wenn das eingehende und ausgehende Field keinen Wei�en K�nig oder Bauer besitzt. 
				if ((vergleich_in != 'b') && (vergleich_out != 'b') && (vergleich_in != 'k') && (vergleich_out != 'k'))
				{
					Edge* edge = new Edge(field[i][j], field[s_in][z_in]);
					g->addEdge(edge);
				}


			}


			p_out = nullptr;
			p_in = nullptr;
			vergleich_in = ' ';
			vergleich_out = ' ';

			//zweimal links einmal unten
			s_in = i - 2;
			z_in = j - 1;

			//Liegt das eingehende Feld (Zielfeld, abgeleitete Node) innerhalb des Schachbretts?
			if ((s_in >= 0) && (s_in <= 7) && (z_in >= 0) && (z_in <= 7))
			{
				//Wenn ja: die mittlere char (out[1][2]) der Darstellungsarray in Eingangs- und Ausgangsfield
				vergleich_out = field[i][j].print(1, 2);
				vergleich_in = field[s_in][z_in].print(1, 2);

				//Edge machen, nur wenn das eingehende und ausgehende Field keinen Wei�en K�nig oder Bauer besitzt. 
				if ((vergleich_in != 'b') && (vergleich_out != 'b') && (vergleich_in != 'k') && (vergleich_out != 'k'))
				{
					Edge* edge = new Edge(field[i][j], field[s_in][z_in]);
					g->addEdge(edge);
				}

			}


			p_out = nullptr;
			p_in = nullptr;
			vergleich_in = ' ';
			vergleich_out = ' ';

			//zweimal rechts einmal unten
			s_in = i + 2;
			z_in = j - 1;

			//Liegt das eingehende Feld (Zielfeld, abgeleitete Node) innerhalb des Schachbretts?
			if ((s_in >= 0) && (s_in <= 7) && (z_in >= 0) && (z_in <= 7))
			{
				//Wenn ja: die mittlere char (out[1][2]) der Darstellungsarray in Eingangs- und Ausgangsfield
				vergleich_out = field[i][j].print(1, 2);
				vergleich_in = field[s_in][z_in].print(1, 2);

				//Edge machen, nur wenn das eingehende und ausgehende Field keinen Wei�en K�nig oder Bauer besitzt. 
				if ((vergleich_in != 'b') && (vergleich_out != 'b') && (vergleich_in != 'k') && (vergleich_out != 'k'))
				{
					Edge* edge = new Edge(field[i][j], field[s_in][z_in]);
					g->addEdge(edge);
				}

			}

			p_out = nullptr;
			p_in = nullptr;
			vergleich_in = ' ';
			vergleich_out = ' ';

			//einmal links zweimal unten
			s_in = i - 1;
			z_in = j - 2;
			//Liegt das eingehende Feld (Zielfeld, abgeleitete Node) innerhalb des Schachbretts?
			if ((s_in >= 0) && (s_in <= 7) && (z_in >= 0) && (z_in <= 7))
			{
				//Wenn ja: die mittlere char (out[1][2]) der Darstellungsarray in Eingangs- und Ausgangsfield
				vergleich_out = field[i][j].print(1, 2);
				vergleich_in = field[s_in][z_in].print(1, 2);

				//Edge machen, nur wenn das eingehende und ausgehende Field keinen Wei�en K�nig oder Bauer besitzt. 
				if ((vergleich_in != 'b') && (vergleich_out != 'b') && (vergleich_in != 'k') && (vergleich_out != 'k'))
				{
					Edge* edge = new Edge(field[i][j], field[s_in][z_in]);
					g->addEdge(edge);
				}

			}


			p_out = nullptr;
			p_in = nullptr;
			vergleich_in = ' ';
			vergleich_out = ' ';

			//einmal rechts zweimal unten
			s_in = i + 1;
			z_in = j - 2;

			//Liegt das eingehende Feld (Zielfeld, abgeleitete Node) innerhalb des Schachbretts?
			if ((s_in >= 0) && (s_in <= 7) && (z_in >= 0) && (z_in <= 7))
			{
				//Wenn ja: die mittlere char (out[1][2]) der Darstellungsarray in Eingangs- und Ausgangsfield
				vergleich_out = field[i][j].print(1, 2);
				vergleich_in = field[s_in][z_in].print(1, 2);

				//Edge machen, nur wenn das eingehende und ausgehende Field keinen Wei�en K�nig oder Bauer besitzt. 
				if ((vergleich_in != 'b') && (vergleich_out != 'b') && (vergleich_in != 'k') && (vergleich_out != 'k'))
				{
					Edge* edge = new Edge(field[i][j], field[s_in][z_in]);
					g->addEdge(edge);
				}

			}

		}
	}
	

}

//Suche auf wei�en Springer
Field Chessboard::findKnight()
{
	char vergleich;

	//Die gesamte field[8][8] in Chessboard wird gelaufen, bis wei�er Springer gefunden ist.
	//Dann wird das dazugeh�rige Feld ausgegeben
	for (int i = 0; i < 8; i++)//spalte
	{
		for (int j = 0; j < 8; j++)//zeile
		{
			vergleich = field[i][j].print(1, 2);

			if (vergleich == 's')
			{
				return field[i][j];



			}

			vergleich = ' ';

		}
	}
}

//Suche auf schwarzer K�nig
Field Chessboard::findKing()
{
	char vergleich;

	//Die gesamte field[8][8] in Chessboard wird gelaufen, bis schwarzer K�nig gefunden ist.
	//Dann wird das dazugeh�rige Feld ausgegeben

	for (int i = 0; i < 8; i++)//spalte
	{
		for (int j = 0; j < 8; j++)//zeile
		{
			vergleich = field[i][j].print(1, 2);

			if (vergleich == 'K')
			{
				return field[i][j];

			}

			vergleich = ' ';
		}
	}
}


//Grafische Darstellung des Schachbretts
stringstream Chessboard::draw()
{
	stringstream ss;
	string s;



	/*
	Die Idee hinter dieser Funktion steckt hinter den Darstellungsarrays aus Chessboard (char printfield[25][41]) und Field (Field.h ->char out[3][5]

	Das Darstellungarray aus Field wird auf DARSTELLUNGSARRAY aus Chessboard �berwiesen

	*/
	//diese Indizen sind f�r DARSTELLUNGSARRAY aus Chessboard. Die �berweisung f�ngt von die linke, obere Seite vom Schachbrett

	//Oberste Zeile
	int i_printfield_zeile = 24;
	//Linkeste Spalte, i_printfield_spalte = 0 ist f�r die Darstellung der Zahlen
	int i_printfield_spalte = 1;

	// diese Indizen sind f�r 
	//int i_field_zeile = 0;
	//int i_field_spalte = 0;


	//i und j Indizen sind f�r field[8][8] aus Chessboard. Die �berweisung f�ngt von die linke, obere Seite vom Schachbrett
	for (int j = 7; j >= 0; j--) //Zeile
	{

		for (int i = 0; i <= 7; i++) //Spalte
		{
			//�berweisung der 1.Zeile von out[3][5] aus Field mit dem oben bestimmten Zeile und Spalte (i,j)
			printfield[i_printfield_zeile][i_printfield_spalte] = field[i][j].print(0, 0);
			printfield[i_printfield_zeile][i_printfield_spalte + 1] = field[i][j].print(0, 1);
			printfield[i_printfield_zeile][i_printfield_spalte + 2] = field[i][j].print(0, 2);
			printfield[i_printfield_zeile][i_printfield_spalte + 3] = field[i][j].print(0, 3);
			printfield[i_printfield_zeile][i_printfield_spalte + 4] = field[i][j].print(0, 4);

			//�berweisung der 2.Zeile von out[3][5] aus Field mit dem oben bestimmten Zeile und Spalte (i,j)
			printfield[i_printfield_zeile - 1][i_printfield_spalte] = field[i][j].print(1, 0);
			printfield[i_printfield_zeile - 1][i_printfield_spalte + 1] = field[i][j].print(1, 1);
			printfield[i_printfield_zeile - 1][i_printfield_spalte + 2] = field[i][j].print(1, 2);
			printfield[i_printfield_zeile - 1][i_printfield_spalte + 3] = field[i][j].print(1, 3);
			printfield[i_printfield_zeile - 1][i_printfield_spalte + 4] = field[i][j].print(1, 4);

			//�berweisung der 3.Zeile von out[3][5] aus Field mit dem oben bestimmten Zeile und Spalte (i,j)
			printfield[i_printfield_zeile - 2][i_printfield_spalte] = field[i][j].print(0, 0);
			printfield[i_printfield_zeile - 2][i_printfield_spalte + 1] = field[i][j].print(0, 1);
			printfield[i_printfield_zeile - 2][i_printfield_spalte + 2] = field[i][j].print(0, 2);
			printfield[i_printfield_zeile - 2][i_printfield_spalte + 3] = field[i][j].print(0, 3);
			printfield[i_printfield_zeile - 2][i_printfield_spalte + 4] = field[i][j].print(0, 4);


			// Nach rechts ziehen, Index f�r DARSTELLUNGSARRAY aus Chessboard um 5 addieren.
			i_printfield_spalte += 5;
		}

		//Nach unten ziehen, um 3 substrahieren
		i_printfield_zeile -= 3;

		//Index f�r DARSTELLUNGSARRAY aus Chessboard  zur�ck nach dem Anfangsposition
		i_printfield_spalte = 1;
	}



	//Fielder grafisch drucken, Zeile pro Zeile
	for (int i = 24; i >= 0; i--)
	{
		for (int j = 0; j <= 40; j++)
		{
			ss << printfield[i][j];

		}
		ss << "\n";
	}

	cout << endl;

	

	return ss;
}
//Das Feld in bestimmter Zeile und Spalte rauskriegen
Field Chessboard::getfield(int i, int j)
{
	return field[i][j]; //spalte x zeile
}

//Der k�rsteste Weg drucken

void Chessboard::printDikjstra()
{
		
	//Eine Deque aus Edges/Wege
		deque <Edge*> weg;

		//Indizen (im Bezug von field[8][8] aus chessboard) f�r Reiter: Zeile, Spalte
		int s_knight, z_knight;

		//Indizen (im Bezug von field[8][8] aus chessboard) f�r K�nig: Zeile, Spalte
		int s_king, z_king;

		s_knight = (int)(findKnight().getcolumn() - 97);
		z_knight = (int)(findKnight().getrow() - 1);

		s_king = (int)(findKing().getcolumn() - 97);
		z_king = (int)(findKing().getrow() - 1);

		//Djikstra ausf�hren
		g->findShortestPathDijkstra(weg, &field[s_knight][z_knight], &field[s_king][z_king]);


		//Schritte drucken
		int schritt = 1;
		cout << endl;

		cout << endl;
		cout << "Schritte vom Reiter zum Koenig: " << endl;
		cout << endl;

		//Fallunterscheidung

		//Es gibt ein Weg! (deque<Edge*> weg ist nicht leer)
		if (weg.size() != 0)
		{
			for (Edge* edge : weg)
			{

				cout << "Schritt " << schritt << " : " << edge->toString() << endl;
				schritt++;
			}
		}
			
		//Es gibt kein Weg (deque<Edge*> weg ist leer)
		else
		{
			cout << "Es gibt kein Weg vom weissen Springer nach schwarzen Koenig."<<endl;
		}
	


	

}




