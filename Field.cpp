#include "Field.h"
#include <iostream>
#include "Piece.h"
#include "King.h"
#include "Pawn.h"
#include "Knight.h"
#include "Node.h"
using namespace std;


void Field::setpiece(Piece* a)
{
	piece = a;

	//�berpr�fe ob das Figur ein 'King' ist -> bei erfolgreicher �berpr�fung (dynamic_cast) wird der K�nig auf dem Schachbrett gesetzt
	try {
		King& king = dynamic_cast<King&>(*piece);

		string s = king.draw().str();

		char ch[1];

		s.copy(ch, s.size() + 1);

		out[1][2] = ch[0];

		
	}
	//wenn die �berweisung/�berpr�fung nicht erfolgreich ist, dann soll keine Ausnahme gel�st werden 

	catch (const bad_cast & e)
	{

	}

	//Ist die �berweisung des K�nigs nicht erfolgreich, dann wird es an dem Springer von dem Piece �berwiesen
	try {
		Knight& knight = dynamic_cast<Knight&>(*piece);

		string s = knight.draw().str();

		char ch[1];
		s.copy(ch, s.size() + 1);

		out[1][2] = ch[0];
		
		
	}
	// ist die �berweisung nicht erfolgreich, dann soll keine Ausnahme gel�st werden
	catch (const bad_cast & e)
	{

	}

	//Ist auch die �berweisung zum Springer nicht erfolgreich, dann wird es an 'Pawn' aus der Klasse Piece �bergeben
	try {
		Pawn& pawn = dynamic_cast<Pawn&>(*piece);
		string s = pawn.draw().str();
		char ch[1];
		s.copy(ch, s.size() + 1);

		out[1][2] = ch[0];
		
		
		
	}
	//ist die �berweisung nicht erfolgreich, dann soll keine Ausnahme gel�st werden
	catch (const bad_cast & e)
	{

	}

}

//die erfolgreich �berwiesene Figur soll ergeben werden

Piece* Field::getpiece()
{
	return piece;
}

//es soll der Inhalt von Array ausgegeben werden
char Field::print(int z, int s)
{
	return out[z][s];
}

//die Zeile ausgegeben werden
unsigned short Field::getrow()
{
	return row;
}

//die Spalte soll ausgegeben werden
char Field::getcolumn()
{
	return column;
}

Field::Field(unsigned short a, char b, Color c, Piece* p, string id) : row(a), column(b), color(c), piece(p), out(), Node(id)
{
	setcolor(c);

}

Field::Field() : row(20), column('z'), color(Color::Black), piece(nullptr), out()
{

}

Field::~Field()
{
	
}

// die Farbe des Feldes soll gesetzt werden
void Field::setcolor(Color c)
{




	//Farbe
	if (color == Color::Black)
	{
		for (int z = 0; z <= 2; z++)
		{
			for (int s = 0; s <= 4; s++)
			{
				if ((z == 1) && (s == 2))
				{
					out[z][s] = '\0';

				}
				else
				{
					out[z][s] = char(0xFF);
				}
			}
		}
	}
	else
	{
		for (int z = 0; z <= 2; z++)
		{
			for (int s = 0; s <= 4; s++)
			{
				if ((z == 1) && (s == 2))
				{
					out[z][s] = '\0';
				}
				else
				{
					out[z][s] = char(0xDB);

				}

			}
		}

	}
}


