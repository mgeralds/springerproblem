/*
Sehr geehrter Herr L�nser,

Das Rumpfprojekt f�r die Bearbeitung
dieses Projekts (als Pr�fungsleistung) stammt aus dem Rumpfprojekt vom Probeklausur (Reiseanwendung) aus Herrn Puschnann.

Die Graphlib, Edge, Graph, Node sind schon drin implementiert.

Das Programm wurde erfolgreich getestet.
Dokumentationen dazu wurden in der Dokumentation.pdf Datei angeh�ngt.

Die Erkl�rungen f�r Field field[8][8] und char printfield[25][41] in Chessboard.h und out[3][5] sind auch in Dokumentation.pdf enthalten.

Vielen Dank.


MfG,

Marchellino Gerald Sutioso, Areeya Wedchasan, Natasya Diposubagio

*/
#include "Chessboard.h"
#include "Graph.h"
#include "Node.h"
#include "Edge.h"
#include "Field.h"
#include <fstream>
#include<iostream>
#include<stdlib.h>

using namespace std;

int main()
{
	//Initialisierungen: chessboard f�r Schachbrett
	Chessboard chessboard;
	//filepath ist der Platz f�r Verzeichnis von Board.txt Datei
	string filepath;
	//s ist der Platz f�r die Ausgabe des Schachbretts
	string s;
	


	//Eingabe des Datenverzeichnisses
	cout << "Geben Sie bitte das Dateiverzeichnis der Board-Datei: ";
	cin >> filepath;
	//Zugriff der Datei in dem eingebebenen Datenverzeichnis
	ifstream Inputfile(filepath.c_str());
	system("cls");
	//initialize-Funktion von chessboard; Input ist 
	chessboard.initialize(Inputfile);
	cout << endl;

	
	//Die zu zeichnendes Schachbrett ist in s eingef�gt.
	s = chessboard.draw().str();
	
	


	//Benutzeroberfl�che 

		//Das Program l�uft, solange der benutzer keine 3 eigibt.
	int auswahl = 0;
	for (int i = 0; auswahl != 3; i++)
	{

		//M�glichkeiten ausgeben
		cout << endl << "Waehlen sie eine der Optionen aus." << endl <<
			"0. Andere Board-Datei Laden" << endl <<
			"1. Das Schachbrett ausdruecken" << endl <<
			"2. Der Dijkstra-Weg anzeigen" << endl <<
			"3. Exit" << endl;
		cout << "Auswahl : ";

		//Auswahl eingeben
		cin >> auswahl;

		system("cls");
		//Fallunterscheidung
		while (auswahl != 3)
		{
			//Wahl = 0 -> Andere Board-Datei laden
			if (auswahl == 0)
			{
				cout << "---------------------------------------------------------------------" << endl;
				//Eingabe des Datenverzeichnisses
				cout << "Geben Sie bitte das neue Dateiverzeichnis der Board-Datei: ";
				cin >> filepath;
				//Zugriff der Datei in dem eingebebenen Datenverzeichnis
				ifstream Inputfile(filepath.c_str());
				system("cls");
				//initialize-Funktion von chessboard; Input ist 
				chessboard.initialize(Inputfile);
				cout << endl;
				
				//Die zu zeichnendes Schachbrett ist in s eingef�gt.
				s = chessboard.draw().str();
				
				cout << "---------------------------------------------------------------------" << endl;
				break;
			}

			//Wahl =1 -> Grafische Darstellung des Schachbretts
			else if (auswahl == 1)
			{
				cout << "---------------------------------------------------------------------" << endl;
				cout << "Graphische Darstellung des Schachbretts" << endl << endl;
				cout << s;
				cout << "---------------------------------------------------------------------" << endl;
				break;
			}

			//Wahl =2 -> K�rzester Weg drucken
			else if (auswahl == 2)
			{
				cout << "---------------------------------------------------------------------" << endl;
				chessboard.printDikjstra();
				cout << "---------------------------------------------------------------------" << endl;
				break;
			}
		}

	}
	
	
	
}