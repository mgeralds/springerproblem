#pragma once
#include "Piece.h"
#include "Color.h"

class Pawn : public Piece
{
public:
	// Funktion zum Erzeugen vom Bauer im Schachbrett
	virtual stringstream draw();
};